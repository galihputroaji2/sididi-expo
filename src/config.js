const defaultAvatar = 'https://ik.imagekit.io/qiyyeqhkum/blank-profile-picture-973460_960_720.png?updatedAt=1709835257914'

const firebaseKey = {
  apiKey: "AIzaSyBN9meArKVKR01KBMfi59EtojGwqc8YohI",
  authDomain: "sididi-app-a21cc.firebaseapp.com",
  projectId: "sididi-app-a21cc",
  storageBucket: "sididi-app-a21cc.appspot.com",
  messagingSenderId: "866646347832",
  appId: "1:866646347832:web:d67bb04f2ed5f3a994d6cd",
  measurementId: "G-PJGDYNBT71"
}

const eulaLink = 'https://github.com/kiyohken2000/ReactNative-Expo-Firebase-Boilerplate-v2'

const expoProjectId = 'd558b5e4-1936-4fee-8622-db4207d6d1c3'

export { defaultAvatar, firebaseKey, eulaLink, expoProjectId }