const colors = {
  darkPurple: '#231d54',
  dark: '#1e1e1e',
  purple: '#7B36FA',
  blueLight: "#788eec",
  lightPurple: '#9388db',
  lightGrayPurple: '#f7f7fb',
  pink: '#ff3d69',
  gray: '#797777',
  grayLight: '#aaaaaa',
  primary: '#92C56D',
  secondary: '#dc143c',
  tertiary: '#228b22',
  white: '#ffffff',
  darkInput: '#303030',
  primaryText: '#2e2e2d',
  lightyellow: '#ffffe0',
  black: '#000000',
}

export default colors
