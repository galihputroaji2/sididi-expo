import React from 'react'
import { LinearGradient } from 'expo-linear-gradient'

const HeaderStyle = () => (
  <LinearGradient
    colors={['#5EA571', '#5EA571']}
    style={{ flex: 1 }}
    start={{x: 0.5, y: 0}}
    end={{x: 1, y: 0}}
  />
)

export default HeaderStyle